let express = require('express');
let app = express();

let mongodb = require('mongodb');

let bodyParser = require('body-parser');
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({
    extended: false
}))

// parse application/json
app.use(bodyParser.json())

const MongoClient = mongodb.MongoClient;
// Connection URL
const uri = "mongodb+srv://rizzaxc:1convit@cluster0-yirxs.gcp.mongodb.net/test?retryWrites=true&w=majority";
//reference to the database & collection
let db;
let col;

const ObjectID = mongodb.ObjectID;

MongoClient.connect(uri, { useNewUrlParser: true },
    function (err, client) {
        if (err) {
            console.log("Err  ", err);
        } else {
            console.log("Connected successfully to server");
            db = client.db("fit2095week6");
            col = db.collection('toDoApp');
        }
    });

app.engine('html', require('ejs').renderFile);
app.set('view engine', 'ejs');

// Serve static files in root/assests and root/views
app.use(express.static('assets'));
app.use(express.static('views'));


app.get('/', function (req, res) {
    res.render('index.html');
});

app.get('/listTask', function (req, res) {
    col.find({}).toArray(function (err, data) {
        if (err) {
            console.log(err);
            res.sendStatus(400);
        }
        else {
            res.render('listTask.html', {db: data});
        }
    });
});

app.route('/deleteTask')
    .get(function (req, res) {
        res.render('deleteTask.html');
    })
    .post(function (req, res) {
        let id = req.body.option;
        let query;
    
        // If the option is set to all, set query to include all tasks
        if (id != 'completed') {
            // try {
            //     query = {_id: ObjectID(id)};
            // } catch (error) {
            //     console.log(error);
            //     res.sendStatus(400);
    
            // }
            query = {_id: ObjectID(id)};
    
        }
        else query = {status: 'completed'};
    
        col.deleteMany(query, function(err, data) {
            if (err) {
                console.log(err);
                res.sendStatus(400);
            }
            else {
                console.log(data.deletedCount);
                res.redirect('/listTask');
            }
        });
    });


app.route('/updateTask')
    .get(function (req, res) {
        res.render('updateTask.html');
    })
    .post(function (req, res) {
        let id = req.body.id;
        let status = req.body.status;
        let query = {_id: ObjectID(id)};
        let change = {$set: {status: status}};
        col.updateOne(query, change, function(err, data) {
            if (err) {
                console.log(err);
                res.sendStatus(400);
            }
            else {
                console.log(data);
                res.redirect('listTask');
            }
        });
    });

app.route('/newTask')
    .get(function (req, res) {
        res.render('newTask.html');
    })
    .post(function (req, res) {
        let task = {
            name: req.body.name,
            dueDate: Date(req.body.dueDate),
            desc: req.body.description,
            assignee: req.body.assignee,
            status: req.body.status,
        };
        col.insertOne(task, function(err, data) {
            if (err) throw err;
            res.redirect('/listTask');
        });
    });



app.route('/insertMany')
    .get(function (req, res) {
        res.render('insertMany.html');
    })
    .post(function (req, res) {
        let count = parseInt(req.body.count);
        let task = {
            // _id: Math.random(),
            name: req.body.name,
            dueDate: Date(req.body.dueDate),
            desc: req.body.description,
            assignee: req.body.assignee,
            status: req.body.status,
        };
        let myArr = [];
        for (let i=0; i<count;i++) {
            // task._id = Math.random();
            myArr.push(task);
            
        }
        col.insertMany(myArr, function(err, data) {
            if (err) throw err;
            console.log("Number of documents inserted: " + data.insertedCount);
            res.redirect('/listTask');

        });
    });

app.listen(8080);